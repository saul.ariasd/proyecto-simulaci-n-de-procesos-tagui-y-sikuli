# Proyecto Simulación de Procesos TagUI y Sikuli

<hr>

## Requerimientos:

- [TagUI](https://github.com/kelaberetiv/TagUI) instalado

## Proceso:

- Posteriormente a descargar y ejecutar TagUI, se debe crear un archivo sin ninguna extension, en este ejemplo se encuentra creado el archivo [proyecto](https://gitlab.com/saul.ariasd/proyecto-simulaci-n-de-procesos-tagui-y-sikuli/-/blob/master/proyectoSimulacion) aquí es en donde se debe agregar todo el proceso que se desee automatizar, para ello puede seguir la [guia de TagUI](https://github.com/kelaberetiv/TagUI), para ejecutar el archivo se debe ejecutar el siguiente comando dentro de la carpeta src de tagUI.

``` 
./tagui samples/proyectoSimulacion chrome
```
- Para ejecutar el proceso para [enviar correo](https://gitlab.com/saul.ariasd/proyecto-simulaci-n-de-procesos-tagui-y-sikuli/-/blob/master/enviarCorreo.sikuli/enviarCorreo.py) se lo debe primero realizarlo abriendo sikuli que se encuentra dentro de src de TagUI. 

![imagen](imagen.png)

## Video Demostrativo:
[aqui](https://www.youtube.com/watch?v=85hSBsZ4XXQ)

